

import Vue from "vue"
import Router from "vue-router"

import { routes as login_routes } from "../apps/login/index"
import { routes as dashboard_routes } from "../apps/dashboard"
import { routes as fail_routes } from "../apps/fail"

Vue.use(Router)

let routes = []
  .concat(login_routes)
  .concat(dashboard_routes)
  .concat(fail_routes)

export default new Router({ routes })
