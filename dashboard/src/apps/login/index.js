
// import components
const login = () => import("./app");
const form = () => import("../../components/login-form/app");


// default routes
const routes = [
	{ path: "/", redirect: "/login" },
	{
		name: "login",
		path: "/login",
		component: login,
		beforeEnter: (to, from, next) => {
			// force session to purge if you visit this page
			localStorage.removeItem("hash");
			next();
		}
	},
]


// vue instance
const vue = {
	props: [],
	mixins: [],
	data(){
		return {}
	},
	components: {
		"loginForm": form
	},
	computed: {
		isForced: function(){
			var bool = this.$route.query.forced;
			var _true = [1, "True", "true", true, "1"];

			if(_true.indexOf(bool) != -1){ return true; }

			return false;
		},
	},
	methods: {},
	mounted(){
		console.log("login: Init!");
	},
	watch: {},
}


export { routes, vue as default }