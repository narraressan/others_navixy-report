
// import components
const dashboard = () => import("./app");
import { SpringSpinner } from "epic-spinners"


// default routes
const routes = [
	{
		name: "dashboard",
		path: "/dashboard/:hash",
		component: dashboard,
		props: true, // pass :hash as a prop on this app (component)
		beforeEnter: (to, from, next) => {
			// check if the hash is different on what is currently in local storage ---> this might be a forced enty
			var hash = localStorage.getItem("hash");

			if(hash != null && hash !== undefined){
				var _hash = hash.replace(/"/g, "");

				if(to.params.hash == _hash) { next(); }
				else{
					console.log("Forced Entry Detected! ");
					next("/login");
				}
			}
			else{
				console.log("No Session detected!");
				next("/login");
			}
		}
	},
]


// vue instance
const vue = {
	props: ["hash"],
	mixins: [],
	data(){
		return {
			search: null,
			date: (new Date).toISOString().split("T")[0],
			trackerList: [],
			trackerGroups: [],
			trackerStates: {},
			trackHistory: {},
			loading: false,
		}
	},
	components: {
		SpringSpinner
	},
	computed: {},
	methods: {
		// TRACKERS ----------------------------------------------------------
		_listTrackers: function(){
			this.loading = true;
			var options = {
				method: "GET",
				url: "http://api.navixy.com/v2/tracker/list",
				params: { hash: this.getLocal("hash") },
				onSuccess: this._onSuccess_TrackerList,
				onError: this._onError_TrackerList,
			};

			this.request(options);
		},
		_onSuccess_TrackerList: function(response){
			if(response.data.success){
				this.trackerList = response.data.list;

				var tracker_ids = [];
				for (var i = 0; i < this.trackerList.length; i++) {
					var tracker_id = this.trackerList[i]["id"];
					tracker_ids.push(tracker_id);
				}

				this._listTrackerStates(tracker_ids);
			}
			this.loading = false;
		},
		_onError_TrackerList: function(err){
			this.trackerList = [];
			this.loading = false;
		},

		// GROUPS ----------------------------------------------------------
		_listTrackerGroups: function(){
			this.loading = true;
			var options = {
				method: "GET",
				url: "http://api.navixy.com/v2/tracker/group/list",
				params: { hash: this.getLocal("hash") },
				onSuccess: this._onSuccess_TrackerGroups,
				onError: this._onError_TrackerGroups,
			};

			this.request(options);
		},
		_onSuccess_TrackerGroups: function(response){
			if(response.data.success){ this.trackerGroups = response.data.list; }
			this.loading = false;
		},
		_onError_TrackerGroups: function(err){
			this.trackerGroups = [];
			this.loading = false;
		},

		// STATES ----------------------------------------------------------
		_listTrackerStates: function(tracker_ids){
			this.loading = true;
			var options = {
				method: "GET",
				url: "http://api.navixy.com/v2/tracker/get_states",
				params: {
					hash: this.getLocal("hash"),
					trackers: JSON.stringify(tracker_ids),
				},
				onSuccess: this._onSuccess_TrackerStates,
				onError: this._onError_TrackerStates,
			};

			this.request(options);
		},
		_onSuccess_TrackerStates: function(response){
			if(response.data.success){ this.trackerStates = response.data.states; }
			this.loading = false;
		},
		_onError_TrackerStates: function(err){
			this.trackerStates = {};
			this.loading = false;
		},

		// TRACKS ----------------------------------------------------------
		_listTracks: function(tracker_id){
			this.loading = true;
			var options = {
				method: "GET",
				url: "http://api.navixy.com/v2/track/read",
				params: {
					hash: this.getLocal("hash"),
					tracker_id: tracker_id,
					from: this.date + " 00:01:00",
					to: this.date + " 23:59:00",
				},
				onSuccess: this._onSuccess_Tracks,
				onError: this._onError_Tracks,
			};

			this.request(options);
		},
		_onSuccess_Tracks: function(response){
			var url = new URL(response.config.url);

			if(response.data.success){
				var _locations = [];

				for (var i = 0; i < response.data.list.length; i++) {
					var tmp = response.data.list[i];
					if(tmp["address"] != "" && tmp["address"] != null){
						_locations.push({
							"timestamp": tmp["get_time"],
							"address": tmp["address"],
							"lat": tmp["lat"],
							"lng": tmp["lng"],
						});
					}
				}

				this.trackHistory[url.searchParams.get("tracker_id")] = _locations;
			}
			this.loading = false;
		},
		_onError_Tracks: function(err){ this.loading = false; },

		// MISC ----------------------------------------------------------
		_group: function(group_id){
			for (var i = 0; i < this.trackerGroups.length; i++) {
				if(this.trackerGroups[i]["id"] == group_id){
					return this.trackerGroups[i]["title"];
				}
			}
			return null;
		},
		_connection: function(tracker_id){
			try{ return this.trackerStates[tracker_id]["connection_status"]; }
			catch(err){ return null; }
		},
		_movement: function(tracker_id){
			try{ return this.trackerStates[tracker_id]["movement_status"]; }
			catch(err){ return null; }
		},
		_firstAddress: function(tracker_id){
			try{ return this.trackHistory[tracker_id][0]["address"]; }
			catch(err){ return null; }
		},
		_firstDate: function(tracker_id){
			try{ return this.trackHistory[tracker_id][0]["timestamp"]; }
			catch(err){ return null; }
		},
		_lastAddress: function(tracker_id){
			try{ return this.trackHistory[tracker_id][(this.trackHistory[tracker_id].length-1)]["address"]; }
			catch(err){ return null; }
		},
		_lastDate: function(tracker_id){
			try{ return this.trackHistory[tracker_id][(this.trackHistory[tracker_id].length-1)]["timestamp"]; }
			catch(err){ return null; }
		},
		refresh: function(){
			this._listTrackers();
			this._listTrackerGroups();
		},
		download: function(){},
		logout: function(){
			this.deleteLocal("hash");
			this.$router.push("/login");
		},
	},
	mounted(){
		console.log("dashboard: Init!");

		this.refresh();
	},
	watch: {},
}


export { routes, vue as default }