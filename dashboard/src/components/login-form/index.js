

// extra components
import { SpringSpinner } from "epic-spinners"


// vue instance
export default {
	name: "Login-Form",
	props: ["forced"],
	mixins: [],
	data(){
		return {
			uname: "pantrucks1@gmail.com",
			pass: "Password1",
			hash: null,
			loading: false,
		}
	},
	components: {
		SpringSpinner
	},
	computed: {},
	methods: {
		_securedLogin: function(){
			this.loading = true;
			var options = {
				method: "GET",
				url: "http://api.navixy.com/v2/user/auth",
				params: {
					login: this.uname,
					password: this.pass,
					dealer_id: 13869
				},
				onSuccess: this.success,
				onError: this.error,
			};

			this.request(options);
		},
		_forcedLogin: function(){
			this.loading = true;
			this.saveLocal("hash", this.hash);
			this.$router.push("/dashboard/" + this.getLocal("hash"));
			this.loading = false;
		},
		success: function(response){
			if(response.data.success){
				var hash = response.data.hash;
				this.saveLocal("hash", hash);
				this.notify("Login Sucessful. I know you!");

				this.$router.push("/dashboard/" + this.getLocal("hash"));
			}
			else{ this.notify("Login Failed", "error"); }

			this.loading = false;
		},
		error: function(error){
			console.log(error);
			this.loading = false;
		}
	},
	mounted(){
		console.log("login-form: Init!");
	},
	watch: {},
}