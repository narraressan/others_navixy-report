// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue"
import App from "./App"
import router from "./router"
import NProgress from "vue-progressbar"
import global_mixin from "./mixin/global"

Vue.config.productionTip = false

Vue.mixin(global_mixin);
Vue.use(NProgress, {
	color: "#15628E",
	failedColor: "#9D0F44",
	thickness: "4px",
	router: true,
});

/* eslint-disable no-new */
new Vue({
	el: "#app",
	router,
	components: { App },
	template: "<App/>"
})
